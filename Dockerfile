FROM alpine/git:latest as fetch-source

WORKDIR /build
COPY .git /build/.git

FROM alpine:latest

COPY --from=fetch-source /build/ /build/
